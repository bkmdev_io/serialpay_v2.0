# README #

Serial Pay is a web application module that eliminates cash handling costs, saves time for queuing at Automated Teller Machines for cash withdrawals and uses serial key or QR codes to handle payment transactions.