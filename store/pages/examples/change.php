
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CP | Account Information</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
          <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                         <ul class="dropdown-menu">
                             <li class="user-footer">
                             <div class="pull-right">
                                <a href="../../logout.php" class="btn btn-default btn-flat">Log out</a>
                             </div>
                             </li>
                         </ul>
                    </li>
               </ul>
          </div>
          </nav>
     </header>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
    Account Information
    </h1>
</section>
<!-- Main content -->
<section class="content">
 <div class="row">
   <div class="col-md-4">
     <!-- Profile Image -->
    <div class="box box-primary">
       <div class="box-body box-profile">
	   <img class="profile-user-img img-responsive img-circle" src="../../dist/img/enterprise-icon.png" alt="User profile picture">

              <h3 class="profile-username text-center">Department Store</h3>
         
</div>
</div>
</div>
<div class="col-md-8">
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Success</a></li>
            </ul>
            <div class='my-fancy-container'>
			<div class="col-md-6"></div>
			
		
    <span style="font-size:6em;" class='my-icon fa fa-check-circle-o'> </span><h1 class="wew" align="center">&nbsp &nbsp &nbsp Success</h1>
	<br>
  <form>
	<label class="label">
             Serial
              
            </label>
			  <div class="callout callout-success">
                <h4>Transaction Finished</h4>
              </div>
    <div class="form-group">
    <div class="col-sm-4"></div>  
    <div class="col-sm-4">
    <a href="department.php" class="btn btn-block btn-lg btn-primary"><b>Home</b></a>
    </div>  
    <div class="col-sm-4"></div>  
    </div>          
    <br><br><br>               
			
	
	</form>
</div>

		
          <style>
		  .my-icon {
    vertical-align: middle;
    font-size: 40px;
	color: green;

}
.wew{
font-size: 50px;
color:green;
}
.my-fancy-container {

    margin: 60px;
    padding: 10px;
}</style>
        </div>  
<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/demo.js"></script>
</body>
</html>
