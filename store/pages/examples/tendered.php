<?php 
    session_start();
    require 'database.php';

  if (!empty($_GET)) {
        $serial = $_GET['serial'];
        $serial = mysqli_real_escape_string($conn, $serial);
        $serial = trim($serial);

        $sql = "SELECT
                  *
                FROM
                  tbl_serial
                WHERE
                  serialNumber = ".$serial.";";

        $result = mysqli_query($conn, $sql);
        // Fetch Records
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
                $serial_id = $row[0];
                $user_id = $row[1];
                $serialNumber = $row[2];
                $amount = $row[3];
                $pending = $row[4];
            }
        } else {
          // echo "<script>window.location.href = 'exist.php';</script>";
        }

        mysqli_close($conn);
      }
    
 ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CP | Account Information</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
          <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                         <ul class="dropdown-menu">
                             <li class="user-footer">
                             <div class="pull-right">
                                <a href="../../logout.php" class="btn btn-default btn-flat">Log out</a>
                             </div>
                             </li>
                         </ul>
                    </li>
               </ul>
          </div>
          </nav>
     </header>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
   <a href="department.php">Transaction Information</a> / 
    Account Information
    </h1>
</section>
<!-- Main content -->
<section class="content">
 <div class="row">
   <div class="col-md-4">
     <!-- Profile Image -->
     <div class="box box-primary">
       <div class="box-body box-profile">
	   <img class="profile-user-img img-responsive img-circle" src="../../dist/img/enterprise-icon.png" alt="User profile picture">

              <h3 class="profile-username text-center">Department Store</h3>
         
</div>
</div>
</div>

        <div class="col-md-8">
          <div class="box">
            <div class="box-header">
              <h2 class="box-title">Transaction Information</h3>
            </div>
			
			<form action="phpScripts/tenderedSubmitProcess.php" method="POST" class="form-horizontal">
          <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
          <input type="hidden" name="serialNumber" value="<?php echo $serialNumber; ?>">
              <div class="box-body">
                <div class="form-group">
                  
				<label for="inputEmail3" class="col-sm-2 control-label">Total Amount Due</label>
                 <div class="input-group col-md-9">
				  <span class="input-group-addon" id="basic-addon1">PHP</span>
				  <input type="number" name="amountDue" class="form-control" placeholder="" aria-describedby="basic-addon1" value="220" readonly="">
				</div>
                </div>
				<div class="form-group">
                  
				<label for="inputEmail3" class="col-sm-2 control-label">Cash Tendered</label>
                 <div class="input-group col-md-9">
				  <span class="input-group-addon" id="basic-addon1">PHP</span>
				  <input type="number" name="cashTendered" class="form-control" placeholder="" aria-describedby="basic-addon1" readonly="" value="<?php echo $amount; ?>">
				</div><br>
        <label for="inputEmail3" class="col-sm-2 control-label">Added Cash</label>
            <div class="input-group col-md-9">
            <span class="input-group-addon" id="basic-addon1">PHP</span>
            <input type="number" name="addedCash" class="form-control" placeholder="" aria-describedby="basic-addon1" value="0">
        </div>        
                </div>
                
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-md-6">
                <button type="submit" class="btn btn-block btn-lg btn-success pull-right">Submit</button><br>
                </div>
                <div class="col-md-6">
                <a href="department.php" class="btn btn-block btn-lg btn-danger pull-right"><b>Cancel Transaction</b></a><br>
              </div><br>
              </div>
              <!-- /.box-footer -->
            </form><br>
            
			
          </div>
		  </div>
		  
		
		  
<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/demo.js"></script>
</body>
</html>
